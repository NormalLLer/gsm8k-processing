import json
import re

from expression_parser import ExpressionParser
from errors import UnknownRightValueError, NoRightValueError, TooManyArgsLeftError
from debug import *


class Sample:
    def __init__(self, index, question, response, answer):
        self.index = index
        self.question = question
        self.response = response
        self.answer = answer
        self.expressions = self._extract_expressions()

    def _extract_expressions(self):
        pattern = r"\$\$([\s\S]*?)\$\$"
        expressions = re.findall(pattern, self.response)
        expressions = [re.sub(r"\$", r"", exp) for exp in expressions]
        return expressions


def get_true_answer(string):
    # 解决 1,200 这种中间有逗号的问题
    return eval(string.replace(r",", ""))


if __name__ == "__main__":
    # 读取数据集
    data_types = ["train", "test"]
    for data_type in data_types:
        data = []
        file_name = f"./data/extracted-data/{data_type}-transferred-new.jsonl"
        print(f"Parsing {data_type}ing data ......")
        with open(file_name, "r") as file:
            for line in file:
                json_data = json.loads(line)
                sample = Sample(json_data["index"], json_data["question"],
                                json_data["response"], json_data["answer"])
                data.append(sample)

        sample_size = len(data)
        print(f"Finished reading data. There are {sample_size} samples in {file_name}.")

        # 处理每个样本并提取表达式的值
        correct_count = 0
        format_error_count = 0

        # meta_data = {
        #     'index': 7,
        #     'question': 'Ken created a care package to send to his brother, who was away at boarding school.  Ken placed a box on a scale, and then he poured into the box enough jelly beans to bring the weight to 2 pounds.  Then, he added enough brownies to cause the weight to triple.  Next, he added another 2 pounds of jelly beans.  And finally, he added enough gummy worms to double the weight once again.  What was the final weight of the box of goodies, in pounds?',
        #     'response': 'Let x be the weight of the box on the scale && (Original Text: @@Ken placed a box on a scale@@)\n$$x = \\text{weight of the box}$$\nLet y be the weight of the jelly beans added to the box initially && (Original Text: @@enough jelly beans to bring the weight to 2 pounds@@)\n$$y = 2 - x$$\nLet z be the weight of the brownies added to the box && (Original Text: @@Then, he added enough brownies to cause the weight to triple@@)\n$$z = 3(x+y) - x - y$$\nLet w be the weight of the gummy worms added to the box && (Original Text: @@And finally, he added enough gummy worms to double the weight once again@@)\n$$w = 2(3(x+y)+2)-x-y-z$$\nLet v be the final weight of the box of goodies && (Original Text: @@What was the final weight of the box of goodies, in pounds?@@)\n$$v = x+y+z+w$$',
        #     'answer': 'To the initial 2 pounds of jelly beans, he added enough brownies to cause the weight to triple, bringing the weight to 2*3=<<2*3=6>>6 pounds.\nNext, he added another 2 pounds of jelly beans, bringing the weight to 6+2=<<6+2=8>>8 pounds.\nAnd finally, he added enough gummy worms to double the weight once again, to a final weight of 8*2=<<8*2=16>>16 pounds.\n#### 16'
        # }
        # sample = Sample(meta_data["index"], meta_data["question"],
        #                         meta_data["response"], meta_data["answer"])

        # data = [data[7139]]
        for sample in data:
            print(f"Sample {sample.index}, ", end='')
            debug_print(sample.response)
            parser = ExpressionParser()
            try:
                for expression in sample.expressions:
                    parser.evaluate_expression(expression)
            except UnknownRightValueError as e:
                format_error_count += 1
                print_error(e.with_traceback(None))
                # print()
                continue
            except NoRightValueError as e:
                format_error_count += 1
                print_error(e.with_traceback(None))
                # print()
                continue
            except ZeroDivisionError as e:
                format_error_count += 1
                print_error(e.with_traceback(None))
                # print()
                continue
            except TooManyArgsLeftError as e:
                format_error_count += 1
                print_error(e.with_traceback(None))
                # print()
                continue

            # 比较表达式的值和答案
            answer_value = get_true_answer(sample.answer.split(" ")[-1])
            if parser.result is not None and int(answer_value) == int(parser.result):  # 只需要最终答案一致
                correct_count += 1
                print(f"Response: {parser.result}, Answer: {answer_value}, Status: Correct")
            else:
                print(f"Response: {parser.result}, Answer: {answer_value}, Status: Wrong")
            # print()

        # 计算 "answer" 字段和 "response" 字段相同的比例
        effective_sample_size = sample_size - format_error_count
        accuracy = correct_count / sample_size if sample_size > 0 else 1
        pure_accuracy = correct_count / effective_sample_size if effective_sample_size > 0 else 1
        print(f"Total sample size: {sample_size}")
        print(f"Effective Sample Size: {effective_sample_size}")
        print(f"Correct Number: {correct_count}")
        print(f"Accuracy (Correct Number / Total sample size): {accuracy}")
        print(f"Accuracy (Correct Number / Effective sample size): {pure_accuracy}")
        print("\n")
