DEBUG = False


def print_error(string):
    print(f"\033[38;5;208m{string}\033[0m")


def debug_print(string):
    if DEBUG:
        print(f"\033[32m{string}\033[0m")
