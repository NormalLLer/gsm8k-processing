import math
import re
from sympy.parsing.latex import parse_latex

from errors import UnknownRightValueError, NoRightValueError, TooManyArgsLeftError
from debug import *


def is_decimal_string(string):
    return re.match(r'^[0-9.]+$', string)


def remove_prefix_zero(string):
    if len(string) == 1:
        return string
    while len(string) > 1 and string[0] == '0' and string[1] != '.':
        string = string[1:]
    return string


class ExpressionParser:
    def __init__(self):
        self.variables = {}
        self.result = None
        self.results = []

    @staticmethod
    def _move_terms(expression):
        # 拆分等式为左右两部分
        left, right = expression.split('=')
        left = left.strip()
        right = right.strip()

        try:
            # 判断左边表达式中的变量是哪个
            if '+' in left:
                variable, constant = left.split('+')
                variable = variable.strip()
                constant = ' - ' + constant.strip()
            elif '-' in left:
                variable, constant = left.split('-')
                variable = variable.strip()
                constant = ' + ' + constant.strip()
            elif '*' in left:
                variable, constant = left.split('*')
                variable = variable.strip()
                constant = ' / ' + constant.strip()
            elif '/' in left:
                variable, constant = left.split('/')
                variable = variable.strip()
                constant = ' * ' + constant.strip()
            else:
                return expression
        except ValueError as e:
            raise TooManyArgsLeftError("More than one operators on the left side of the '='")

        # 将表达式移项
        new_expression = f"{variable} = {right} {constant}"
        debug_print(f"Before moving items: {expression}")
        debug_print(f"After moving items:")
        return new_expression

    def evaluate_expression(self, expression):

        # FIXME: 由于题目的解都不是区间，因此直接将边界条件作为等式
        expression = re.sub(r"\\*[gl]*eq", r" = ", expression)
        # print(expression)

        if "=" not in expression:
            expression = f"r = {expression}"
            # raise NoRightValueError("No right value error.")

        expression = " = ".join(expression.split("=")[0:2])
        expression = self._move_terms(expression)

        expression = re.sub(r"\\{2}", r'\\', expression)
        parts = expression.split("=")
        left_var_name = parts[0].strip()
        right_value = parts[1].strip()
        if "unknown" in right_value:
            raise UnknownRightValueError("Unknown right value 'unknown'.")
        self._evaluate_right_value(left_var_name, right_value)

    def _evaluate_right_value(self, var_name, expression):
        expression = self._handle_latex(expression)  # 处理 \frac{}{}, {} \times {}

        tokens = expression.split()  # 得到中缀表达式
        post_fix = self._infix_to_postfix(tokens)  # 中缀转后缀表达式
        self.result = self._evaluate_postfix(post_fix)
        self.results.append(self.result)

        # # 将 tokens 中所有已声明变量替换
        # new_exp = []
        # for token in tokens:
        #     token = self.variables.get(token, token)
        #     new_exp.append(str(token))

        var_name = self._remove_braces(var_name)
        self.variables[var_name.strip()] = self.result
        debug_print(f"Store variable {var_name}: {self.variables[var_name]}")

    @staticmethod
    def _handle_latex(expression):

        def handle_frac(expression):
            # 将"\frac{x}{y}"替换为 "x / y", 将 "\times" 替换为 "*"
            expression = re.sub(r"\\x0crac", r"\\frac", expression)
            expression = re.sub(r"\x0crac", r"\\frac", expression)
            expression = re.sub(r"\brac\b", r"\\frac", expression)
            expression = re.sub(r"\\*over", r" / ", expression)

            # 处理 \frac{x}{y}, [^}]: 匹配除了 "}" 以外的任意字符
            expression = re.sub(r"\\+frac{([^}]+)}{([^}]+)}", r" ( ( \1 ) / ( \2 ) ) ", expression)
            return expression

        def handle_times(expression):
            expression = re.sub(r"\bimes\b", r"\\times", expression)
            expression = re.sub(r"\\+times", r" * ", expression)
            return expression

        def handle_ceil_floor(expression):
            # FIXME: 直接移除 \lfloor、\floor 和 \rfloor
            # FIXME: 直接移除 \lceil、 \ceil 和 \rceil
            expression = re.sub(r"\\*ceil\((.+?)\)", r" \1 ", expression)
            expression = re.sub(r"\\[lr]floor", r' ', expression)
            expression = re.sub(r"\\[lr]ceil", r' ', expression)
            return expression

        expression = handle_frac(expression)
        expression = handle_times(expression)
        expression = handle_ceil_floor(expression)

        pattern0 = r"\{\{([\d\w]+)\}\}"  # {{}} → {}
        pattern1 = r"max\((.+?),\s*(.+?)\)"
        pattern2 = r"\\+div"  # 处理 \div
        pattern3 = r"\\+left(.+?)\\+right"
        pattern4 = r"\\+cdot"  # 处理 \cdot
        pattern5 = r"([\d\.]+)([A-Za-z]+)"  # 处理数字和字母连在一起的乘法: 2y => 2 * y
        pattern6 = r'([()+\-*/])'  # 在所有"("、")"、"+"、"-"、"*"、"/"的前后分别加一个空格
        pattern7 = r'[{}]'  # 去除所有 {}
        pattern8 = r'\\%|%'  # 替换百分号
        pattern9 = r"min\((.+?),\s*(.+?)\)"
        pattern10 = r','  # 移除逗号
        pattern11 = r'\^'  # 在 ^ 两侧加空格
        pattern12 = r"([\d\.\w]+)\s*\("  # 数字左括号、字母左括号：中间补乘号
        pattern13 = r"\)\s*([\d\.\w]+)"  # 右括号数字、右括号字母：中间补乘号
        pattern14 = r"\)\s*\("  # ) ( => ) * (
        pattern15 = r"million"  # 去掉 million 这个单位

        debug_print(f"before handling: {expression}")
        # try:
        #     expression = parse_latex(expression)
        # except Exception as e:
        #     print(expression)
        #     exit()
        expression = re.sub(pattern0, r"{ \1 }", expression)
        expression = re.sub(pattern1, r" ( ( \1 ) : ( \2 ) )", expression)  # :表示max
        expression = re.sub(pattern9, r" ( ( \1 ) ; ( \2 ) )", expression)  # ;表示max
        expression = re.sub(pattern2, r" / ", expression)
        expression = re.sub(pattern3, r" ( \1 ) ", expression)
        expression = re.sub(pattern4, r" * ", expression)
        expression = re.sub(pattern5, r" ( ( \1 ) * ( \2 ) ) ", expression)
        expression = re.sub(pattern6, r' \1 ', expression)
        expression = re.sub(pattern7, r' ', expression)
        expression = re.sub(pattern8, r' * 0.01 ', expression)
        expression = re.sub(pattern10, '', expression)
        expression = re.sub(pattern11, r' ^ ', expression)
        expression = re.sub(pattern12, r' ( \1 ) * ( ', expression)
        expression = re.sub(pattern13, r' ) * ( \1 ) ', expression)
        expression = re.sub(pattern14, r' ) * ( ', expression)
        expression = re.sub(pattern15, r'', expression)

        debug_print(f"after handling: {expression}")

        return expression

    @staticmethod
    def _remove_braces(expression):  # 去除大括号
        pattern5 = r'[{}]'  # 去除所有 { }
        return re.sub(pattern5, r'', expression).strip()

    def _infix_to_postfix(self, expression):
        precedence = {';': 1, ':': 1, '+': 1, '-': 1, '*': 2, '/': 2, '^': 3}
        postfix = []
        stack = []
        debug_print(f'\ninfix: {expression}')
        # for item in expression:
        #     if (not item.isdigit()) and (item not in self.variables.keys()):
        #         raise UnknownRightValueError(f"Unknown right value {item}")
        debug_print(self.variables)
        for char in expression:
            if char == '(' and len(expression) > 1:
                stack.append(char)
            elif char == ')' and len(expression) > 1:
                while stack and stack[-1] != '(':
                    postfix.append(stack.pop())
                stack.pop()  # 弹出左括号 '('
            elif char in [';', ':', '+', '-', '*', '/', '^'] and len(expression) > 1:
                # debug_print(f"stack: {stack}")
                while stack and stack[-1] != '(' and precedence[char] <= precedence.get(stack[-1], 0):
                    postfix.append(stack.pop())
                stack.append(char)
            elif is_decimal_string(char):
                postfix.append(char)
            elif char in self.variables.keys():
                # (char not in self.variables.keys()) and not (char.isdigit() or is_decimal_string(char)):
                char = self.variables.get(char.strip(), char)  # 替换已定义变量
                postfix.append(char)
            elif char.isalpha():
                # 特殊情况，如果字母串中的每个字母都是已知变量，则将它们相乘
                right, num = True, 1
                for ch in char:
                    if ch in self.variables.keys():
                        num *= self.variables.get(ch)
                    else:
                        right = False
                        break
                if right:
                    postfix.append(num)
                else:
                    # postfix.append(999.999)
                    raise UnknownRightValueError(f"Unknown right value '{char}'.")
            else:
                # postfix.append(999.999)
                raise UnknownRightValueError(f"Unknown right value '{char}'.")

        while stack:
            postfix.append(stack.pop())

        return postfix

    @staticmethod
    def _evaluate_postfix(postfix):
        debug_print(f"postfix: {postfix}")
        stack = []

        for char in postfix:
            if type(char) == int or type(char) == float:
                stack.append(char)
            elif is_decimal_string(char):  # 只有数字和"."
                stack.append(eval(remove_prefix_zero(char)))
            else:
                debug_print(f"stack: {stack}")
                debug_print(f"operator: {char}")
                operand2 = stack.pop()
                if len(stack) != 0:
                    operand1 = stack.pop()
                else:
                    operand1 = '0'  # 可能是单目运算符，如负号
                operand1, operand2 = str(operand1), str(operand2)
                if eval(operand1) == Ellipsis or eval(operand2) == Ellipsis:
                    stack.clear()  # 有省略号，算不来
                    break
                if char == '^':
                    result = math.pow(eval(operand1), eval(operand2))
                elif char == ':':
                    debug_print(f"max({operand1}, {operand2})")
                    result = eval(operand1) if eval(operand1) > eval(operand2) else eval(operand2)
                elif char == ';':
                    debug_print(f"min({operand1}, {operand2})")
                    result = eval(operand2) if eval(operand1) > eval(operand2) else eval(operand1)
                else:
                    result = eval(operand1 + char + operand2)
                stack.append(result)

        if len(stack) == 0:  # 等式右侧没东西
            return 999.999
        return stack.pop()
