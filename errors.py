class UnknownRightValueError(Exception):
    """右值不知道"""

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class NoRightValueError(Exception):
    """没有右值，即式子中没有等号"""

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class TooManyArgsLeftError(Exception):

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message
