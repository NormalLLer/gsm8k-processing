import json
import re
import sys

from expression_parser import ExpressionParser


class OriginalSample:
    """
    读取的正确的数据，每一个 OriginalSample 对象对应一条数据
    """

    def __init__(self, index, final_answer, question, response):
        self.index = index
        self.final_answer = final_answer
        self.question = question

        # 统一 LaTex 格式
        self.original_response = response
        self.response = OriginalSample._format_latex(response)
        # # 提取所有的变量定义句子
        # self.var_def_text_list = self._extract_variable_definition_text_list()
        # # 提取所有原文内容
        # self.original_text_list = self._extract_original_text_list()
        # # 提取所有公式
        # self.expression_list = self._extract_expression_list()

        # 提取所有的变量定义句子，所有原文内容，所有公式
        self.var_def_text_list, self.original_text_list, self.expression_list = self.extract()

        self.answers = self.get_all_answers()

        self.sequence_size = len(self.original_text_list)
        # self.sequence_size = min(len(self.original_text_list), len(self.answers))

        # print(len(self.var_def_text_list))
        # print(len(self.answers))

    def extract_expressions_from_original_response(self):
        pattern = r"\$\$([\s\S]*?)\$\$"
        expressions = re.findall(pattern, self.original_response)
        expressions = [re.sub(r"\$", r"", exp) for exp in expressions]
        return expressions

    def get_all_answers(self):
        parser = ExpressionParser()
        try:
            expressions = self.extract_expressions_from_original_response()
            for expression in expressions:
                # print(expression.strip('$$'))
                parser.evaluate_expression(expression)
        except Exception as e:
            print(e.with_traceback(None), file=sys.stderr)
            raise e
            pass
        return parser.results

    def extract(self):
        pattern = r"(.+?) && \(Original Text: @@(.+?)@@\)\s+(\$\$.+?\$\$)"
        data = re.findall(pattern, self.response)
        if len(data) == 0:
            return [], [], []
        return zip(*data)

    def get_input_for_sequence_num(self, seq_num: int):
        # 获取要输出数据的 'input'：由所在 sequence 的原文内容以及之前所有变量定义 + 原文组成
        inputs = ""
        for i in range(seq_num):
            inputs += self.var_def_text_list[i] + ', ' + self.original_text_list[i] + '.\n'
        inputs += self.original_text_list[seq_num] + '. '

        return inputs

    def get_output_for_sequence_num(self, seq_num: int):
        # 获取要输出数据的 'output'：有所在 sequence 的变量定义 + 表达式组成
        output = self.var_def_text_list[seq_num] + ', ' + self.expression_list[seq_num]
        return output

    # def _extract_variable_definition_text_list(self):
    #     """
    #     :return: 所有变量定义句子 "Let ....."
    #     """
    #     pattern = r"Original Text: @@([\s\S]*?)@@\)"
    #     var_def_text_list = re.findall(pattern, self.response)
    #     var_def_text_list = [re.sub(r"@", r"", exp).strip() for exp in var_def_text_list]
    #     return var_def_text_list
    #
    # def _extract_original_text_list(self):
    #     """
    #     :return: 所有被 @@ 包围的文本
    #     """
    #     pattern = r"Original Text: @@([\s\S]*?)@@\)"
    #     original_text_list = re.findall(pattern, self.response)
    #     original_text_list = [re.sub(r"@", r"", exp).strip() for exp in original_text_list]
    #     return original_text_list
    #
    # def _extract_expression_list(self):
    #     """
    #     :return: 所有被 $$ 包围的文本
    #     """
    #     pattern = r"\$\$([\s\S]*?)\$\$"
    #     expressions = re.findall(pattern, self.response)
    #     expressions = [re.sub(r"\$", r"", exp).strip() for exp in expressions]
    #     return expressions

    @staticmethod
    def _format_latex(expression):
        expression = re.sub(r'\n+', '\n', expression).strip()

        expression = re.sub(r"\\x0crac", r"\\frac", expression)
        expression = re.sub(r"\x0crac", r"\\frac", expression)
        expression = re.sub(r"\brac\b", r"\\frac", expression)

        expression = re.sub(r"\\*over", r"\\over", expression)

        expression = re.sub(r"\bimes\b", r"\\times", expression)

        expression = re.sub(r"\\*ceil\((.+?)\)", r" \1 ", expression)
        expression = re.sub(r"\\[lr]floor", r' ', expression)
        expression = re.sub(r"\\[lr]ceil", r' ', expression)

        pattern0 = r"\{\{([\d\w]+)\}\}"  # {{}} → {}
        pattern1 = r"max\((.+?),\s*(.+?)\)"
        pattern2 = r"\\+div"  # 处理 \div
        pattern3 = r"\\+left(.+?)\\+right"
        pattern4 = r"\\+cdot"  # 处理 \cdot
        pattern5 = r"([\d\.]+)([A-Za-z]+)"  # 处理数字和字母连在一起的乘法: 2y => 2 * y
        pattern9 = r"min\((.+?),\s*(.+?)\)"
        pattern10 = r','  # 移除逗号
        pattern12 = r"([\d\.\w]+)\s*\("  # 数字左括号、字母左括号：中间补乘号
        pattern13 = r"\)\s*([\d\.\w]+)"  # 右括号数字、右括号字母：中间补乘号
        pattern14 = r"\)\s*\("  # ) ( => ) * (
        pattern15 = r"million"  # 去掉 million 这个单位

        expression = re.sub(pattern0, r"{ \1 }", expression)
        expression = re.sub(pattern1, r" ( ( \1 ) : ( \2 ) )", expression)  # :表示max
        expression = re.sub(pattern9, r" ( ( \1 ) ; ( \2 ) )", expression)  # ;表示max
        expression = re.sub(pattern2, r"\\div", expression)
        expression = re.sub(pattern3, r" ( \1 ) ", expression)
        expression = re.sub(pattern4, r"\\times", expression)
        expression = re.sub(pattern5, r"((\1) \\times (\2))", expression)
        expression = re.sub(pattern10, '', expression)
        expression = re.sub(pattern12, r' ( \1 ) \\times ( ', expression)
        expression = re.sub(pattern13, r' ) \\times ( \1 ) ', expression)
        expression = re.sub(pattern14, r' ) \\times ( ', expression)
        expression = re.sub(pattern15, r'', expression)

        expression = re.sub(r'\t', r'', expression)

        return expression


class FormattedSample:
    """
    要输出的数据，每一个 FormattedSample 对应原来一个样本中的一个表达式;
    index 为 FormattedSample 对象的序号;
    sample_index 为所在原来样本的序号;
    sequence_index 为在原来样本中公式的序号，即第几个公式;
    output 为原来样本中的第 sequence_index 条公式以及公式前的 "Let ......";
    input 为原来样本中该公式 output 所应的原文和之前所有的原文 + "Let .....";
    """
    index = -1

    @staticmethod
    def __distribute_index():
        FormattedSample.index += 1
        return FormattedSample.index

    def __init__(self, sample_index, sequence_index, final_answer, input_list, output):
        self.index = FormattedSample.__distribute_index()
        self.sample_index = sample_index
        self.sequence_index = sequence_index
        self.final_answer = final_answer
        self.input_list = input_list
        self.output = output

    def to_dict(self):
        return {
            "index": self.index,
            "sample_index": self.sample_index,
            "sequence_index": self.sequence_index,
            "final_answer": self.final_answer,
            "input": self.input_list,
            "output": self.output
        }


if __name__ == "__main__":

    # 读取数据集
    data_types = ["train", "test"]

    formatted_samples = {
        'train': [],
        'test': []
    }

    for data_type in data_types:
        file_name = f"./data/extracted-correct-data/correct_data-{data_type}-new.jsonl"
        print(f"Formatting correct data from {data_type}ing data ......")
        with open(file_name, "r") as file:
            for line in file:
                json_data = json.loads(line)
                original_sample = OriginalSample(
                    index=json_data['index'],
                    final_answer=json_data['final_answer'],
                    question=json_data['input'],
                    response=json_data['output']
                )
                start_index = len(formatted_samples[data_type])
                end_index = start_index + original_sample.sequence_size
                for i in range(original_sample.sequence_size):
                    formatted_sample = FormattedSample(
                        sample_index=original_sample.index,
                        sequence_index=i + 1,
                        final_answer=original_sample.answers[i],
                        input_list=original_sample.get_input_for_sequence_num(i),
                        output=original_sample.get_output_for_sequence_num(i)
                    )
                    formatted_samples[data_type].append(formatted_sample)
                print(f"Original Sample {original_sample.index} => "
                      f"Formatted Samples (Size: {original_sample.sequence_size}; "
                      f"Index Range: {start_index, end_index})")
        print(f"Finished formatting {data_type}ing data, there are "
              f"{FormattedSample.index} formatted samples\n\n")

    # 输出格式化后的数据到 "formatted_correct_data-train/test.jsonl"
    for data_type in data_types:

        output_file = f"./data/format-extracted-correct-data/" \
                      f"formatted_correct_data-{data_type}.jsonl"

        with open(output_file, "w") as outfile:
            for entry in formatted_samples[data_type]:
                json.dump(entry.to_dict(), outfile)
                outfile.write('\n')
